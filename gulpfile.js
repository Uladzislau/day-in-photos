var gulp = require('gulp'),
    minifyCss = require('gulp-minify-css'),
    less = require('gulp-less'),
    watch = require('gulp-watch');

gulp.task('default', function () {
    return gulp.src('src/less/common.less')
        .pipe(less())
        .pipe(minifyCss())
        .pipe(gulp.dest('app/css'));
});

gulp.task('gulpDev', function () {
    return gulp.src('src/less/common.less')
        .pipe(less())
        .pipe(gulp.dest('app/css'));
});

gulp.task('watchDev', function (cb) {
    watch('src/less/*.less', function () {
        gulp.src('css/**/*.css')
            .pipe(less())
            .pipe(gulp.dest('app/css'))
            .on('end', cb);
    });
});